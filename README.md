# tackle-shop

A collection of git hooks organized by type and function.  Use for inspiration,
functional use, testing code before pushing to remote repositories.


## Navigating This Repository

There's a directory for each type of git hook type.  In this repository, all
scripts with specific functions are named and in the directory that aligns to
their best use.  For scripts that are language specifc, the script name should
be prepended with the specific language.  In the case of scripts for generic
use, no language prefix is required.

If a specific script has a configuration file associated with it, the filename
should be appended with `.config` and any filetype extension after that.


## Using These Scripts

To use these scripts, copy them into the appropriate hook script.  If the user
has made no configurations to their git installation, there should be a
selection of sample scripts in each git project's `hooks` directory.

```
ls ./.git/hooks
applypatch-msg.sample      pre-applypatch.sample      pre-push.sample
commit-msg.sample          pre-commit.sample          pre-rebase.sample
fsmonitor-watchman.sample  pre-merge-commit.sample    pre-receive.sample
post-update.sample         prepare-commit-msg.sample  update.sample
```

Copy the contents of the desired script to the corresponding `*.sample` file,
remove the `*.sample` file extension, and verify that the script is
executable.
